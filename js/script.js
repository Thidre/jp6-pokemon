
function talk(id, text, onfinish) {
    var e = document.getElementById(id);
    var chars = text.split("");
    function addChar() {
        var c = chars.shift();
        if (c === '\n') {
            e.innerHTML += '<span class="glyphicon glyphicon-triangle-bottom talk-toggle"></span>'
            $(e).click(function() { $(e).unbind('click'); $(e).html(""); talk(id, chars.join(""), onfinish); });
        }
        else {
            e.innerHTML += c;
            if (chars.length >0)
                setTimeout(addChar, 30);
            else if (onfinish !== null)
                onfinish();
        }
    }
   setTimeout(addChar, 30);
}

