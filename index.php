<?php
require_once(__DIR__.'/Model/JPlayer.php');
require_once(__DIR__.'/Model/DatabaseInterface.php');

$authFailed = false;
$pseudoExists = false;
$playerDoesNotExists = false;
$creationFailed = false;

if (key_exists("new_pseudo", $_POST)) {
    // Registration
    $pseudo = htmlspecialchars($_POST["new_pseudo"]);
    $password = htmlspecialchars($_POST["new_password"]);
    $db = new DataBaseInterface();
    $pseudoExists = $db->getJPlayerByPseudo($pseudo) != NULL;
    if (!$pseudoExists) {
        $player = JPlayer::createPlayer($pseudo, $password);
        $creationFailed = $db->insertJPlayer($player) > 0;
        if (!$creationFailed) {
            setcookie("jpok_guid", $player->getTokenGuid());
            header('Location:tutorial');
            exit();
        }
    }
}   
if (key_exists("re_pseudo", $_POST)) {
    // Connection
    $pseudo = htmlspecialchars($_POST["re_pseudo"]);
    $password = htmlspecialchars($_POST["re_password"]);
    $db = new DataBaseInterface();
    $player = $db->getJPlayerByPseudo($pseudo);
    $playerDoesNotExists = $player == NULL;
    if (!$playerDoesNotExists) {
        $authFailed = $player->getPasswordHash() != md5($password);
        if (!$authFailed) {
            setcookie("jpok_guid", $player->getTokenGuid());
            header('Location:menu');
            exit();
        }
    }
}   
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Pokemon Party</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="./css/styles.css" />
        <link rel="icon" href="favicon.ico" />
	<script type="text/javascript" src="js/bootstrap.min.js" ></script>
    </head>
    <body class="index-back">
       <div class="jumbotron light-white text-center">
           <img src="img/index-title.png" class="index-title" />
           <img src="img/logopokemon.png" class="index-logopokemon" />
       </div>
       <div class="container">
           <?php
           if ($authFailed) 
               { ?> <div class="alert alert-danger "><b>L'authentification a échoué.</b> Veuillez vérifier votre mot de passe.</div> <?php }
           else if ($playerDoesNotExists) 
               { ?> <div class="alert alert-danger "><b>L'authentification a échoué.</b> Aucun joueur avec ce pseudo n'a été trouvé.</div> <?php }
           else if ($pseudoExists) 
               { ?> <div class="alert alert-danger "><b>L'inscription a échoué.</b>Votre pseudo est déjà utilisé. Merci d'en chosiir un autre.</div> <?php }
           else if ($creationFailed) 
               { ?> <div class="alert alert-danger "><b>La création a échoué pour une raison inconnue. Veuillez contacter un responsable. Merci.</b></div> <?php }
           ?>
           
           <div class="panel col-xs-8 col-xs-offset-2 semilight-white">
               <h3>Rejoindre la compétion</h3>
               <form class="index-form" method="post" action="index.php">
                    <div class="form-group">
                      <label for="new_pseudo">Pseudo</label>
                      <input type="text" class="form-control" name="new_pseudo" id="new_pseudo" placeholder="Pseudo">
                    </div>
                    <div class="form-group">
                      <label for="new_password">Mot de passe</label>
                      <input type="password" class="form-control" name="new_password" id="new_password">
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary">GO !</button>
                    </div>
                </form>
           </div>
             <div class="panel col-xs-8 col-xs-offset-2 semilight-white">
               <h3>Repartir en chasse</h3>
               <form class="index-form" method="post" action="index.php">
                    <div class="form-group">
                      <label for="re_pseudo">Pseudo</label>
                      <input type="text" class="form-control" id="re_pseudo"  name="re_pseudo"  placeholder="Pseudo">
                    </div>
                    <div class="form-group">
                      <label for="re_password">Mot de passe</label>
                      <input type="password" class="form-control" name="re_password"  id="re_password">
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary">GO !</button>
                    </div>
                </form>
           </div>
       </div>
    </body>
</html>
