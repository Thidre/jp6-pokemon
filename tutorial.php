<!DOCTYPE html>
<html>
    <head>
         <title>Pokemon Party</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="./css/styles.css" />
        <link rel="icon" href="favicon.ico" />
        <script type="text/javascript" src="js/jquery-1.12.0.min.js" ></script>
        <script type="text/javascript" src="js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="js/script.js" ></script>
    </head>
    <body class="standard-bg">
        <audio controls autoplay>
         <source src="assets/tuto-sound.mp3">
         </audio>
        <img src="img/chen.png" class="tuto-chen" />
        <form method="post" action="menu" class="container col-xs-12 col-sm-8 col-sm-offset-2">
            <table class="poke-dialog-old">
                <tr><td class="poke-dialog-old-corner poke-dialog-old-upleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-upright"></td></tr>
                 <tr><td class="poke-dialog-old-vedge"></td><td id="talk" class=""></td><td class="poke-dialog-old-vedge"></td></tr>
                 <tr><td class="poke-dialog-old-corner poke-dialog-old-downleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-downright"></td></tr>
            </table>
            <div class="container col-xs-11 col-xs-offset-1 col-sm-6 col-sm-offset-6 col-md-3 col-md-offset-9" style="padding:0px">
             <table class="poke-dialog-old">
                <tr><td class="poke-dialog-old-corner poke-dialog-old-upleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-upright"></td></tr>
                 <tr><td class="poke-dialog-old-vedge"></td><td><button type="submit">JE SUIS PRET !</td><td class="poke-dialog-old-vedge"></td></tr>
                 <tr><td class="poke-dialog-old-corner poke-dialog-old-downleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-downright"></td></tr>
            </table>
            </div>
 
        </form>
         <script type="text/javascript">
            talk('talk', "Bienvenue Dresseur! Je suis le professeur Chen, le plus connu des professeurs Pokémon!\n"
        + "Nos amis de Japan Party ont souhaité mettre en place cette année un concours pour fêter les 20 ans du départ de Sacha du Bourg Palette.\n"
        + "Avec mes assistants nous avons accepté de leur prêter quelques uns de nos pokémon en pension. Laissez moi vous expliquer les règles de ce safari:\n"
        + "Mes assistants ont réparti dans le salon des QR codes permettant d'accéder à une zone contenant chacune un pokémon. En les scannant, vous aurez\n"
        + "La possibilité de les rencontrer. Il vous faudra une application (non fournie) pour scanner les QR codes. Attention, tous les pokémons ne sont pas\n"
        + "aussi facile à capturer... Si vous réussissez, vous gagnerez des points. Si vous échouez, vous pourrez toujours retenter de capturer le pokémon un\n"
        + "peu plus tard... Si jamais vous capturez tous les pokémons, Allez trouver un responsable pour récupérer votre lot! Si personne n'y arrive, les\n"
        + "candidats seront départagés en fonction des points. Bonne chance, Dresseur. un monde d'aventures et de découvertes s'ouvre à toi!", null);
        </script>
    </body>
   
</html>
