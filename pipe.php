<?php

session_start();
srand(time());
// Redirect unknown commands.
if(!key_exists("do", $_GET)) {
     include(__DIR__.'/404.html');
     exit();
}

if (!key_exists("jpok_guid", $_COOKIE)) {
    include(__DIR__.'/index.php');
    exit();
}

switch($_GET["do"]) {
    case "home":        include(__DIR__.'/index.php'); exit(); break;
    case "tutorial":    include(__DIR__.'/tutorial.php'); exit(); break;
    case "menu":        include(__DIR__.'/card.php'); exit(); break;
    case "trycatch":    include(__DIR__.'/catch.php'); exit(); break;
    case "disconnect":  setcookie("jpok_guid", null, -1);  include(__DIR__.'/index.php'); exit(); break;
    default:            include(__DIR__.'/404.html'); exit(); break;
}
?>
