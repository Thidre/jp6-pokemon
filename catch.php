<?php
require_once(__DIR__.'/Model/JPlayer.php');
require_once(__DIR__.'/Model/Pokemon.php');
require_once(__DIR__.'/Model/DatabaseInterface.php');

$db = new DataBaseInterface();
$player = $db->getJPlayerByToken($_COOKIE['jpok_guid']);
$pokemonFound = null;
$pokemonIsInvalid = false;
$pokemonGuidFound = $_GET['pokemon'];
$alreadyCaught = false;
$notRespawnYet = false;
$respawnTimeLeft = 0;
// First, let's decide if mew appears.
if (!$player->isPokemonsCaught(Pokemon::MEW_ID) && rand(0, 100) <= Pokemon::MEW_APPEARANCE_PROB)
    // Mew appears! 
    $pokemonFound = $db->getPokemon(Pokemon::MEW_ID);
else
    $pokemonFound = $db->getPokemonByGuid ($pokemonGuidFound);
$pokemonIsInvalid = $pokemonFound == null;
if (!$pokemonIsInvalid) {
    $alreadyCaught = $player->isPokemonsCaught($pokemonFound->getId());
    if ($alreadyCaught)
        // you cannot find twice the same pokemon.
        $pokemonFound = null;
    else {
        // player can't retry if pokemon has not respawn. tries are sorted by date desc.
        $tries = $db->getAllTries($player->getId(), $pokemonFound->getId());
        $respawnTimeLeft = empty($tries)? 0 : $pokemonFound->getRespawnTime() - (time() - $tries[0]->getDate()->getTimestamp());
        $notRespawnYet = $respawnTimeLeft > 0;
        if ($notRespawnYet) 
            // pokemon has not respawn yet.
            $pokemonFound = null;
    }
}
// if pokemon has been found, let's decide if it is caught.
if ($pokemonFound != null) {
    $isCaught = rand(0,100) <= $pokemonFound->getCatchPercentage();
    if ($isCaught) {
        $player->addPokemonsCaught($pokemonFound->getId());
        $db->updateJPlayer($player);
    }
    $db->insertCatchTry(new CatchTry($player->getId(), $pokemonFound->getId(), $isCaught, new DateTime()));
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Pokemon Party</title>
       <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="../css/styles.css" />
        <link rel="icon" href="../favicon.ico" />
        <script type="text/javascript" src="../js/jquery-1.12.0.min.js" ></script>
	<script type="text/javascript" src="../js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="../js/script.js" ></script>
    </head>
    <body class="standard-bg">
        <div class="container col-xs-12 container col-sm-8 col-sm-offset-2">
         <table class="poke-dialog-old">
                <tr><td class="poke-dialog-old-corner poke-dialog-old-upleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-upright"></td></tr>
                 <tr><td class="poke-dialog-old-vedge"></td><td style="background-color:#EFEFEF" class="catchWindow" id="catchWindow"></td><td class="poke-dialog-old-vedge"></td></tr>
                 <tr><td class="poke-dialog-old-corner poke-dialog-old-downleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-downright"></td></tr>
            </table>
        </div>
         <div class="container col-xs-12 container col-sm-8 col-sm-offset-2">
            
        
            <table class="poke-dialog-old">
                <tr><td class="poke-dialog-old-corner poke-dialog-old-upleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-upright"></td></tr>
                 <tr><td class="poke-dialog-old-vedge"></td><td id="talk"></td><td class="poke-dialog-old-vedge"></td></tr>
                 <tr><td class="poke-dialog-old-corner poke-dialog-old-downleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-downright"></td></tr>
            </table>
        </div>
         <script type="text/javascript">
             function gotoMenu() { 
                window.location = "../menu"; 
            }
              <?php  if ($pokemonFound != null) { ?>
            function launchPokeball() {
                $('#catchWindow').html("<img src='../img/<?php echo $isCaught? $pokemonFound->getCatchAnimation() : $pokemonFound->getFailAnimation();?>' alt='try-catch'></img>");
                 <?php  if ($isCaught) { ?> 
                     setTimeout(function() { $('body').after('<audio src="../assets/catch.mp3" autoplay></audio>'); talk('talk', 'Super ! Le pokemon a été capturé !\n Essayez de les trouver tous !\n ', gotoMenu); }, 5600); 
                         <?php } else  { ?> 
                     setTimeout(function() { talk('talk', 'Oh non, le pokémon s\'est libéré !\n Une autre fois peut-être...\n ', gotoMenu); }, 4500);
                 <?php } ?>
                 }
            <?php
            } ?>
            function main() {
              <?php
              if ($notRespawnYet) {
                ?>   talk('talk', "Il n'y a pas de pokémon pour le moment ici. Revenez dans <?php echo $respawnTimeLeft ?> secondes...\n ", gotoMenu);
            <?php } else if ($pokemonIsInvalid) {
                ?>   talk('talk', "Cette zone ne fait pas partie du safari Japan Party... Vous devriez faire demi-tour !\n ", gotoMenu);
            <?php } else if ($alreadyCaught) {
                ?>   talk('talk', "Vous avez déjà capturé le pokémon de cette zone... Mieux vaut aller chercher ailleurs !\n ", gotoMenu);
           <?php } else {
                ?>   talk('talk', "Un <?php echo $pokemonFound->getName()?> sauvage apparait !\n" + "<?php echo $player->getName()?> lance une Japan Ball !\n ", launchPokeball);
            <?php } ?>
                }
                $("body").ready(main);
        </script>
    </body>
</html>

