<?php
require_once(__DIR__.'/Model/JPlayer.php');
require_once(__DIR__.'/Model/Pokemon.php');
require_once(__DIR__.'/Model/DatabaseInterface.php');

$db = new DataBaseInterface();
$pokemons = $db->getAllPokemons();
$player = $db->getJPlayerByToken($_COOKIE['jpok_guid']);
$score = 0;
foreach($pokemons as $pokemon)
    if ($player->isPokemonsCaught($pokemon->getId()))
        $score += $pokemon->getPoints();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Pokemon Party</title>
       <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="./css/styles.css" />
        <link rel="icon" href="favicon.ico" />
	<script type="text/javascript" src="js/bootstrap.min.js" ></script>
    </head>
    <body class="standard-bg">
         <div class="container col-xs-12 container col-sm-8 col-sm-offset-2">
            <table class="poke-dialog-old">
                <tr><td class="poke-dialog-old-corner poke-dialog-old-upleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-upright"></td></tr>
                 <tr><td class="poke-dialog-old-vedge"></td><td class="card-dresser">
                     <img src="img/red.png" class="card-red" />
                     NOM/ <?php echo $player->getName(); ?><br/>
                     POKE./ <?php echo count($player->getPokemonsCaught()) ?>/20<br/>
                     TEMPS/ <?php echo $player->getSpentTime()->format(JPlayer::APPLI_FORMAT); ?><br/>
                     SCORE/ <?php echo $score; ?>
                     </td><td class="poke-dialog-old-vedge"></td></tr>
                 <tr><td class="poke-dialog-old-corner poke-dialog-old-downleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-downright"></td></tr>
            </table>
          <form method="post" action="disconnect">
          <table class="poke-dialog-old">
                <tr><td class="poke-dialog-old-corner poke-dialog-old-upleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-upright"></td></tr>
                <tr><td class="poke-dialog-old-vedge"></td><td><button type="submit">DECONNEXION</button></td><td class="poke-dialog-old-vedge"></td></tr>
                 <tr><td class="poke-dialog-old-corner poke-dialog-old-downleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-downright"></td></tr>
            </table>
          </form>
            <div class="container col-xs-12" style="padding:0px">
             <table class="poke-dialog-old">
                <tr><td class="poke-dialog-old-corner poke-dialog-old-upleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-upright"></td></tr>
                 <tr><td class="poke-dialog-old-vedge"></td><td>
                         <?php
                        
                         foreach($pokemons as $pokemon) { 
                             if ($player->isPokemonsCaught($pokemon->getId())) {
                                  ?><img src="img/<?php echo $pokemon->getCauchtIconName(); ?>" class="poke-icon" /><?php  
                             }
                             else if ($pokemon->getId() != Pokemon::MEW_ID) {
                                  ?><img src="img/<?php echo $pokemon->getUncauchtIconName();?>" class="poke-icon" /><?php 
                             }
                         }?>
                 <td class="poke-dialog-old-vedge"></td></tr>
                <tr><td class="poke-dialog-old-corner poke-dialog-old-downleft"></td><td class="poke-dialog-old-hedge"></td><td class="poke-dialog-old-corner poke-dialog-old-downright"></td></tr>
            </table>
            </div>
 
        </div>
    </body>
</html>
