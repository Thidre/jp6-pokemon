<?php
require_once(__DIR__.'/JPlayer.php');
require_once(__DIR__.'/Pokemon.php');

/**
 * Maps Business model objects to their SQL representation.
 *
 * @author Thibault
 */
abstract class SqlMapper {
    abstract function SqlMapper();
    
    /**
     * Maps a JPlayer to a dictionary of its members.
     * @param JPlayer $player
     * @returns String[]
     */
    static public function FromJPlayer($player) {
        return array(
            "id"            => sprintf("%d", $player->getId()),
            "name"          => $player->getName(),
            "password"      => $player->getPasswordHash(),
            "guid"          => $player->getTokenGuid(),
            "pokemons"      => implode(",", $player->getPokemonsCaught()),
            "creation_date" => $player->getCreationDate()->format(JPlayer::DB_FORMAT)
        );
    }
    /**
     * Build a JPlayer from a dictionary of its members.
     * @param String[] $f
     * @return JPlayer
     */
    static public function ToJPlayer($f) {

        return new JPlayer(
                $f["id"], 
                $f["name"], 
                $f["password"], 
                $f["guid"],
                $f["pokemons"] == null? array() : explode(",", $f["pokemons"]),
                DateTime::createFromFormat(Pokemon::DB_FORMAT, $f["creation_date"]));
    }
     
    /**
     * Build a Pokemon from a dictionary of its members.
     * @param String[] $f
     * @return Pokemon
     */
    static public function ToPokemon($f) {

        return new Pokemon(
            $f["id"], 
            $f["name"], 
            (int)$f["points"], 
            (int)$f["percentage"],
            (int)$f["respawn_time"],
            $f["guid"]);
        }
    /**
     * Maps a CatchTry to a dictionary of its members.
     * @param CatchTry $catchTry
     * @returns String[]
     */
    static public function FromCatchTry($catchTry) {
        return array(
            "player_id"    => sprintf("%d", $catchTry->getPlayerId()),
            "pokemon_id"   => sprintf("%d", $catchTry->getPokemonId()),
            "is_success"   => sprintf("%d", $catchTry->isSuccess()),
            "date"         => (new DateTime())->format(Pokemon::DB_FORMAT)
        );
    }    
    /**
     * Build a CatchTry from a dictionary of its members.
     * @param String[] $f
     * @return CatchTry
     */
    static public function ToCatchTry($f) {

        return new CatchTry(
                $f["player_id"], 
                $f["pokemon_id"], 
                (bool)$f["is_success"], 
                DateTime::createFromFormat(Pokemon::DB_FORMAT, $f["date"]));
        }
    
}
?>
