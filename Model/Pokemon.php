<?php


/**
 * Pokemon infos in database.
 *
 * @author Thibault
 */
class Pokemon {
   const DB_FORMAT = "Y-m-d H:i:s";
   const MEW_ID = 8;
   const MEW_APPEARANCE_PROB = 10;
    /**
     * Its id.
     * @var int 
     */
   private $id;
   /**
    *Name of this pokemon.
    * @var String 
    */
   private $name;
   /**
    * Points it gives to its dresser.
    * @var int 
    */
   private $points;
   /**
    * Percentage to catch that pokemons.
    * @var int 
    */
   private $catchPercentage;
   /**
    * Respawn time in seconds for each player.
    * @var int 
    */
   private $respawnTime;
    /**
    * Unique GUID for this pokemon.
    * @var String 
    */
   private $guid;
   
   public function getId() { return $this->id; }
   
   public function getName() { return $this->name; }
   
   public function getPoints() { return $this->points; }
   
   public function getCatchPercentage() { return $this->catchPercentage; }
    
   public function getRespawnTime() { return $this->respawnTime; }
   
   public function getCauchtIconName() { return $this->name.'.png'; }
   
   public function getUncauchtIconName() { return $this->name.'-u.png'; }
   
   public function getCatchAnimation() { return $this->name.'.gif'; }
   
   public function getFailAnimation() { return $this->name.'-fail.gif'; }
   
   public function getGuid() { return $this->guid; }
    
   public function Pokemon($id, $name, $points, $catchPercentage, $respawnTime, $guid) {
        $this->id = $id;
        $this->name = $name;
        $this->points = $points;
        $this->catchPercentage = $catchPercentage;
        $this->respawnTime = $respawnTime;
        $this->guid = $guid;
   }
   
}

?>
