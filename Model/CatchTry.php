<?php

/**
 * A time a player tried to catch a pokemon.
 *
 * @author Thibault
 */
class CatchTry {
    /**
     * Player Id.
     * @var int
     */
    private $playerId;
    /**
     * Pokemon Id.
     * @var int
     */
    private $pokemonId;
    /**
     * Tells if player succeed or not.
     * @var bool
     */
    private $isSuccess;
    /**
     * Try date and time.
     * @var DateTime
     */
    private $date;
    
    public function getPlayerId() { return $this->playerId; }
    
    public function getPokemonId() { return $this->pokemonId; }
     
    public function isSuccess() { return $this->isSuccess; }
      
    public function getDate() { return $this->date; }
    
    public function CatchTry($playerId, $pokemonId, $isSuccess, $date) {
       $this->playerId = (int)$playerId;
       $this->pokemonId = (int)$pokemonId;
       $this->isSuccess = (bool)$isSuccess;
       $this->date = $date;
    }
}

?>
