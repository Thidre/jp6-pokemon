<?php

/**
 * Gathers what is necessary to establish a connection with a database.
 *
 * @author Thibault
 */
class ConnectionInfo {
    
    /**
     * Creates a new connection with provided params.
     * @param String $pcHost_ Host
     * @param String $pcDataBase_ Database
     * @param String $pcLogin_ Login
     * @param String $pcPassword_ Password
     */
    public function ConnectionInfo($pcHost_, $pcDataBase_, $pcLogin_, $pcPassword_) {
        $this->pcHost = $pcHost_;
        $this->pcDatabase = $pcDataBase_;
        $this->pcLogin = $pcLogin_;
        $this->pcPassword = $pcPassword_;
    }
    
    /**
     * Gets Host.
     * @return String
     */
    public function Host() { return $this->pcHost == null ? "" : $this->pcHost; }
    /**
     * Gets Database.
     * @return String
     */
    public function Database() { return $this->pcDatabase == null ? "" : $this->pcDatabase; }
    /**
     * Gets Password.
     * @return String
     */
    public function Password() { return $this->pcPassword == null ? "" : $this->pcPassword; }
    /**
     * Gets Login.
     * @return String
     */
    public function Login() { return $this->pcLogin == null ? "" : $this->pcLogin; }
    
    /**
     * Creates a new connection from params and returns it.
     * @return PDO
     */
    public function Connect() {
            return new PDO(
                    sprintf("mysql:host=%s;dbname=%s", $this->Host(), $this->Database()),
                    $this->Login(),
                    $this->Password(),
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } 
    
    /**
     * Database host, like "localhost" or 192.168.1.1.
     * @var String
     * 
     */
    private  $pcHost;
    /**
     * Authentication login.
     * @var String
     */
    private $pcLogin;
    /**
     * Authentication password.
     * @var String
     */
    private $pcPassword;
    /**
     * Database name.
     * @var String
     */
    private $pcDatabase;
}

?>
