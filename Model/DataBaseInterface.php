<?php
require_once(__DIR__.'/JPlayer.php');
require_once(__DIR__.'/Pokemon.php');
require_once(__DIR__.'/CatchTry.php');
require_once(__DIR__.'/ConnectionInfo.php');
require_once(__DIR__.'/SqlMapper.php');

/**
 * Manage database access.
 *
 * @author Thibault Drevon
 */
class DataBaseInterface {
    
    /**
     * Default connection. Used for debugging mainly.
     * @var ConnectionInfo
     */
    private static $CONNECTIONINFO_DEFAULT;
    
    /**
     * Creates a new requester on database.
     */
    public function DatabaseInterface() {
        if (self::$CONNECTIONINFO_DEFAULT == null)
         self::$CONNECTIONINFO_DEFAULT = new ConnectionInfo("localhost", "JPokemon", "root", "root");
    }

    private function Connection() { 
        $this->connection = $this->connection == null ? self::$CONNECTIONINFO_DEFAULT : $this->connection;
        return $this->connection;
    }
    
    /**
     * Get all items from provided table.
     * @param String $pcTableName Database table name.
     * @param Function $pfToMapper Function pointer to appropriate mapper.
     * @return Object[] array of resulting objects.
     */
    private function getAll($pcTableName, $pfToMapper) {
        $request = $this->Connection()->Connect();
        $statement = $request->prepare("SELECT * FROM ".$pcTableName);
        $statement->execute();
        $res = array();
        while ($fetch = $statement->fetch())
            $res[] = call_user_func($pfToMapper, $fetch);
        $statement->closeCursor();
        return $res;
    }
    
     /**
     * Get item with provided id from provided table.
     * @param String $pcTableName Database table name.
     * @param Function $pfToMapper Function pointer to appropriate mapper.
     * @return Object resulting object.
     */
    private function getOne($pcTableName, $pfToMapper, $id) {
        $request = $this->Connection()->Connect();
        $statement = $request->prepare("SELECT * FROM ".$pcTableName. " WHERE id=".$id);
        $statement->execute();
        $res = null;
        while ($fetch = $statement->fetch())
            $res = call_user_func($pfToMapper, $fetch);
        $statement->closeCursor();
        return $res;
    }
    
     /**
     * Get items that match condition.
     * @param String $pcTableName Database table name.
     * @param Function $pfToMapper Function pointer to appropriate mapper.
     * @return Object[] array of resulting objects.
     */
    private function getWhere($pcTableName, $pfToMapper, $whereClause) {
        $request = $this->Connection()->Connect();
        $statement = $request->prepare("SELECT * FROM ".$pcTableName. " WHERE ".$whereClause);
        $statement->execute();
        $res = array();
        while ($fetch = $statement->fetch())
            $res[] = call_user_func($pfToMapper, $fetch);
        $statement->closeCursor();
        return $res;
    }
    
    
    /**
     * 
     * @param String $pcTableName Database table name.
     * @param Function $pfFromMapper Function pointer to appropriate mapper.
     * @param Object $object Object to insert.
     * @return int Last inserted id.
     * @throws Exception If something goes wrong.
     */
    private function insert($pcTableName, $pfFromMapper, $object) {
        $request = $this->Connection()->Connect();
        $insertPart = "";   // Builds insert clause, fields are coma separated.
        $valuesPart = "";   // Builds value clause, also coma separated. Add a ':' because it is parameterized.
        $params = array();  // Parameters applied.
        $mapped = call_user_func($pfFromMapper, $object);
        foreach ($mapped as $key => $value) {
            if ($key == 'id')
                // in an insert, skip id field.
                continue;
            $insertPart.= $key.',';
            $valuesPart.= ':'.$key.',';
            $params[':'.$key] = $value;
        }
        $insertPart = trim($insertPart, ',');
        $valuesPart =  trim($valuesPart, ',');
        $statement = $request->prepare(sprintf("INSERT INTO %s (%s) VALUES (%s);", $pcTableName, $insertPart, $valuesPart));
        if (!$statement->execute($params))
            throw new Exception("Insert failed.");
        $statement->closeCursor();
        return (int)$request->lastInsertId();
    }
    
      /**
     * 
     * @param String $pcTableName Database table name.
     * @param Function $pfFromMapper Function pointer to appropriate mapper.
     * @param Object $object Object to insert.
     * @throws Exception If something goes wrong.
     */
    private function update($pcTableName, $pfFromMapper, $object) {
        $request = $this->Connection()->Connect();
        $query = "";   // Builds update clause, pairs are coma separated.
        $params = array();  // Parameters applied.
        $mapped = call_user_func($pfFromMapper, $object);
        foreach ($mapped as $key => $value) {
            if ($key != 'id')
                $query.= sprintf("%s = :%s,", $key, $key);
            $params[':'.$key] = $value;
        }
        $query = trim($query, ',');
        $statement = $request->prepare(sprintf("UPDATE %s SET %s WHERE id = :id;", $pcTableName, $query));
        if (!$statement->execute($params))
            var_dump ($statement->errorInfo ());
        $statement->closeCursor();
    }
    
    /**
     * Delete element with specified id from table.
     * @param type $pcTableName Database table name.
     * @param type $id id of object to delete.
     * @throws Exception If something goes wrong.
     */
    private function delete($pcTableName, $id) {
        $request = $this->Connection()->Connect();
        $statement = $request->prepare(sprintf("DELETE FROM %s WHERE article_id = :id;", $pcTableName));
        if (!$statement->execute(array(":id" => $id)))
             var_dump ($statement->errorInfo ());
        $statement->closeCursor();
    }
    
    /**
     * Delete element with specified id from table.
     * @param type $pcTableName Database table name.
     * @param type $id id of object to delete.
     * @throws Exception If something goes wrong.
     */
    private function deleteAll($pcTableName) {
        $request = $this->Connection()->Connect();
        $statement = $request->prepare(sprintf("TRUNCATE TABLE %s;", $pcTableName));
        if (!$statement->execute())
            throw new Exception("Truncate failed");
        $statement->closeCursor();
    }
    
    /**
     * Gets one JPlayer.
     * @return JPlayer
     */   
    public function getJPlayer($id) { return $this->getOne("JPLAYERS", 'SqlMapper::ToJPlayer', $id); }
    /**
     * Gets one JPlayer by token
     * @return JPlayer
     */   
    public function getJPlayerByToken($token) { 
        $players = $this->getWhere("JPLAYERS", 'SqlMapper::ToJPlayer', "guid='$token'");
        if (empty($players))
            return null;
        return $players[0];
    }
      /**
     * Gets one JPlayer by $pseudo
     * @return JPlayer
     */   
    public function getJPlayerByPseudo($pseudo) { 
        $players = $this->getWhere("JPLAYERS", 'SqlMapper::ToJPlayer', "name='$pseudo'");
         if (empty($players))
            return null;
        return $players[0];
    }
     /**
     * Gets all the JPlayers.
     * @return JPlayer[]
     */   
    public function getAllJPlayers() { return $this->getAll("JPLAYERS", 'SqlMapper::ToJPlayer'); }
    
    /*
     * Update provided player.
     * @param JPlayer $player
     */
    public function updateJPlayer($player) { $this->update("JPLAYERS", 'SqlMapper::FromJPlayer', $player); }
    /*
     * Insert provided player.
     * @param JPlayer $player
     */
    public function insertJPlayer($player) { $this->insert("JPLAYERS", 'SqlMapper::FromJPlayer', $player); }
    /**
     * Gets all the Pokemons.
     * @return Pokemons[]
     */   
    public function getAllPokemons() { return $this->getAll("POKEMONS", 'SqlMapper::ToPokemon'); }
     /**
     * Gets one Pokemon by guid
     * @return Pokemon
     */   
    public function getPokemonByGuid($guid) { 
        $pokemons = $this->getWhere("POKEMONS", 'SqlMapper::ToPokemon', "guid='$guid'");
        if (empty($pokemons))
            return null;
        return $pokemons[0];
    }
      /**
     * Gets one Pokemon by id
     * @return Pokemon
     */   
    public function getPokemon($id) { return $this->getOne("POKEMONS", 'SqlMapper::ToPokemon', $id); }
     /**
     * Gets All tries of a player for a specific pokemon.
     * Tries are sorted by date desc. 
     * @return CatchTry[]
     */   
    public function getAllTries($playerId, $pokemonId) { return $this->getWhere("CATCH_TRIES", 'SqlMapper::ToCatchTry', "player_id=$playerId AND pokemon_id=$pokemonId ORDER BY date DESC"); }
    /**
     * insert a CatchTry in database.
     * @param CatchTry $catchTry
     * @return int new object id.
     */
    public function insertCatchTry($catchTry) { $this->insert("CATCH_TRIES", 'SqlMapper::FromCatchTry', $catchTry); return $catchTry; }
    /**
     * Connection to database.
     * @var ConnectionInfo 
     */
    private $connection;
}

?>
