<?php
   
/**
 * Contains details about time a cosplayer should go.
 * Also contains its current state (Comes into VIP room, etc...)
 *
 * @author Thibault
 */
class JPlayer {
    
    const DB_FORMAT = "Y-m-d H:i:s";
    const APPLI_FORMAT = "H:i";
    
    /**
     * JPlayer unique id.
     * @var int 
     */
    private $id;
    /**
     * Player login. Will be displayed in game.
     * @var String
     */
    private $name;
    /**
     * Cosplayer password hash. Used to identify him.
     * @var String
     */
    private $passwordHash;
    /**
     * token used to identify user in cookies.
     * @var String
     */
    private $tokenGUID;
    /**
     * List of pokemon caught.
     * @var int[]
     */
    private $pokemonsCaught;
    
    /**
     * Date and time player was created.
     * @var DateTime
     */
    private $creationDate;

    private function createUniqueId() { return bin2hex(openssl_random_pseudo_bytes(16)); }

    public function getId() { return $this->id; }
    
    public function setId($iId) { $this->id = $iId; }
    
    public function getPasswordHash() { return $this->passwordHash; }
    
    public function setPassword($password) { $this->password = md5($password); }
    
    public function setPasswordHash($passwordHash) { $this->passwordHash = $passwordHash; }
     
    public function getName() { return $this->name; }
    
    public function setName($name) { $this->name = $name; }
    
    public function getTokenGuid() { return $this->tokenGUID; }
    
    public function setTokenGuid($token) { $this->tokenGUID = $token; }
    
    public function getPokemonsCaught() { return $this->pokemonsCaught; }
    /**
     * 
     * @param type $pokemonId
     * @return bool
     */
    public function isPokemonsCaught($pokemonId) { return in_array($pokemonId, $this->pokemonsCaught); }
    
    public function addPokemonsCaught($pokemonId) { return $this->pokemonsCaught[] = $pokemonId; }
    /**
     * 
     * @return DateTime
     */
    public function getCreationDate() { return $this->creationDate; }
    
    public function setCreationDate($creationDate) { $this->creationDate = $creationDate; }
    /**
     * 
     * @return DateTime
     */
    public function getSpentTime() { $dt = new DateTime(); $dt->setTimestamp(time() - $this->creationDate->getTimestamp()); return $dt; }
    
    /**
     * Creates a new JPlayer.
     * @param type $name
     * @param type $password
     * @return JPlayer
     */
    public static function createPlayer($name, $password) {
        return new JPlayer(0, $name, md5($password), JPlayer::createUniqueId(), array(), new DateTime());
    }
    /**
     * Creates a new JPlayer.
     * 
     */
    public function JPlayer($id, $name, $passwordHash, $tokenGUID, $pokemonsCaught, $creationDate) {
        $this->id = (int) $id;
        $this->name = $name;
        $this->passwordHash = $passwordHash;
        $this->tokenGUID = $tokenGUID;
        $this->pokemonsCaught = $pokemonsCaught == null ? array() : $pokemonsCaught;
        $this->creationDate = $creationDate;
    }
}

?>
