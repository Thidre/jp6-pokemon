-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 29 Février 2016 à 21:30
-- Version du serveur :  5.6.17-log
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `jpokemon`
--
CREATE DATABASE IF NOT EXISTS `jpokemon` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `jpokemon`;

-- --------------------------------------------------------

--
-- Structure de la table `catch_tries`
--

CREATE TABLE IF NOT EXISTS `catch_tries` (
  `player_id` int(11) NOT NULL,
  `pokemon_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_success` tinyint(1) NOT NULL,
  PRIMARY KEY (`player_id`,`pokemon_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `jplayers`
--

CREATE TABLE IF NOT EXISTS `jplayers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `guid` varchar(255) NOT NULL,
  `pokemons` varchar(512) DEFAULT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `guid` (`guid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `pokemons`
--

CREATE TABLE IF NOT EXISTS `pokemons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `points` int(11) NOT NULL,
  `percentage` int(11) NOT NULL,
  `respawn_time` int(11) NOT NULL,
  `guid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `guid` (`guid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Contenu de la table `pokemons`
--

INSERT INTO `pokemons` (`id`, `name`, `points`, `percentage`, `respawn_time`, `guid`) VALUES
(1, 'Abra', 1, 50, 50, '23688982-ec87-4ed5-b4e5-19e01ac21da4'),
(2, 'Bulbizarre', 1, 50, 50, 'c8f3e623-6868-48e1-a3cf-854f3f480c84'),
(3, 'Canarticho', 1, 50, 50, 'b77628ed-ee9b-44bf-b65a-805ddfe43e27'),
(4, 'Carapuce', 1, 50, 50, 'a15f8315-988e-40be-a38d-0c85b308bef3'),
(5, 'Evoli', 1, 50, 50, '311c38eb-6700-44ff-bd5d-87d8654c6bf6'),
(6, 'Fantominus', 1, 50, 50, 'e78be5fd-db79-428c-a520-468ed3185e0a'),
(7, 'Insecateur', 1, 50, 50, '6768805b-620f-4222-b8c3-fab96c08c58f'),
(8, 'Mew', 1, 50, 50, '8d3b5558-a90b-4e15-a8f5-8f779e45796d'),
(9, 'Miaouss', 1, 50, 50, '5cef8090-739d-4fbb-97a4-2a555e716c7a'),
(10, 'Mr.Mime', 1, 50, 50, '236f3f04-f3a0-4eac-b142-a66d78363dfe'),
(11, 'Onix', 1, 50, 50, '3c3c42a3-7b2d-4d04-9aec-be6e950aab47'),
(12, 'Ortide', 1, 50, 50, '2ee2ccd8-98a7-4ce9-a38a-bd909bbcdcf0'),
(13, 'Otaria', 1, 50, 50, 'bc549838-201f-48eb-bdfe-3f186a30dc7c'),
(14, 'Papillusion', 1, 50, 50, '7fd97f8d-6014-4563-8ffb-15776c0ceff9'),
(15, 'Pikachu', 1, 50, 50, '231e02a5-9e4c-4f56-ba3e-6432bbdcb2b0'),
(16, 'Ponyta', 1, 50, 50, 'f7d841cb-ee2b-4dec-8106-bfcfba01eb20'),
(17, 'Ramoloss', 1, 50, 50, '6521399e-c38b-4f6d-8630-1b7edd1c25e3'),
(18, 'Rondoudou', 1, 50, 50, 'a8a13c11-a951-4dd9-ad43-b5c25948fb59'),
(19, 'Roucool', 1, 50, 50, '5855f508-6b09-46ab-ae7c-fb3ebaf16efa'),
(20, 'Salameche', 1, 50, 50, 'd0afabde-a233-46cf-a221-d1a2b502224d'),
(21, 'Voltorbe', 1, 50, 50, '0da886ea-fa8c-4898-b98f-2db19b78ce5e');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
